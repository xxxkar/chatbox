CHATBOX 9.30.19
by: Karla Jean

Web Technology used: Angularjs, Php, Rachet(websocket) and MySql

Instruction:
For XAMPP:
	1. Open Xampp and start Apache and MySql.
	2. Add sakura_chatbox db name.
	3. Import sakura_chatbox db located in /c/xampp/htdocs/chatbox.
	4. Done.
For websocket:
	1. Open Cmd and locate chatbox directory e.g. /c/xampp/htdocs/chatbox
	2. Type "php bin/chat-server.php"
	3. Done. Then, wait for connections.
For testing:
	1. Open browser.
	2. Type http://localhost/chatbox/index.html#/
	3. Done.
