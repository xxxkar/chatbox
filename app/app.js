
(function() {

	angular.module('app', [
		'UserController',
		'ui.router',
		'UserService',
		'RachetService',
		'ChatController'
		
	])

	.config(function($stateProvider,$urlRouterProvider) {

		var login = {
			name: 'login',
			url: '/',
			templateUrl: 'resources/views/login.html',
			title: 'Login Page',
			controller: 'UserController as user'
		}
		var chat = {
			name: 'chat',
			url: '/chat',
			templateUrl: 'resources/views/chat.html',
			title: 'Chat Page',
			controller: 'ChatController as chat'
		}
		

		$urlRouterProvider.otherwise('/');

		$stateProvider
		.state(login)
		.state(chat);
		

	});
	

}) ();
