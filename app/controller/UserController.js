(function() {

	angular.module('UserController', ['UserService','RachetService'])
	.controller('UserController', ['$scope','$rootScope', '$state', 'UserService', 'RachetService',
		function($scope,$rootScope,$state,UserService,RachetService) {

			"use strict";
			console.log('UserController');

			var vm = this;
			$scope.loginForm = {};
			$scope.chatForm = {};
			$scope.loginFailed= false;
			$scope.registerAccount = false;
			$scope.logoutUser = false;

			$scope.signLogin = function() {

				$rootScope.loading = true;
				console.log( vm.loginForm.username+vm.loginForm.password);
				//login failed if blank inputs
				if(vm.loginForm.username == NaN || vm.loginForm.password == NaN) {
					$scope.loginFailed = true;	
				}
				else {
					var data = {
						action: 'login',
						username:vm.loginForm.username,
						password: vm.loginForm.password
					};
					console.log(data);

					UserService.checkUser(data)
					.then(function(response){
						console.log('response',response);
						if(response['status'] == 'login') { //login success
						    $scope.logoutUser = true;	
							$scope.loginFailed= false;
							sessionStorage.username = JSON.stringify(response['username']);
							console.log(sessionStorage.username);
							$state.go('chat');

						}
						else { //registerd success
						    $scope.registerAccount = true;	
							$scope.loginFailed= false;
							$scope.logoutUser = true;	
						    $state.go('login');
						} 
						
					})
					.catch(function(error){
						console.log('error:',error);
					});
				}
				$rootScope.loading = false;	
			}

		}]);
}) ();