(function() {

	angular.module('ChatController', ['RachetService'])
	.controller('ChatController', ['$scope','$rootScope', '$state', 'RachetService',
		function($scope,$rootScope,$state,RachetService) {

			"use strict";
			console.log('ChatController');

			var vm = this;
		
			$scope.chatForm = {};

			//open new connection for websocket	-- Rachet
			var conn = new WebSocket('ws://localhost:8080');
			conn.onopen = function(e) {
			    console.log("Connection established!");
			};

			conn.onmessage = function(e) {
				    console.log(e.data);
				    var leftHtml = 	'<div class="row">'+
										'<div id="left-message">'+ e.data +'</div>'+
										'<div id="left-name">Anonymous</div>'+
									'</div>';
				    angular.element(document.getElementById("wrapper")).append(leftHtml);
				    
				};

			$scope.sendMessage = function() {
				//console.log('send');
				var data = {
					message: vm.chatForm.message
				}

				console.log(JSON.parse(sessionStorage.username));

				conn.onmessage = function(e) {
				    console.log(e.data);
				    var leftHtml = 	'<div class="row">'+
										'<div id="left-message">'+ e.data +'</div>'+
										'<div id="left-name">Anonymous</div>'+
									'</div>';
				    angular.element(document.getElementById("wrapper")).append(leftHtml);
				    
				};
				//right message
				conn.send(data['message']);
				var html = 	'<div class="row">'+
								'<div id="message">'+ data['message'] +'</div>'+
								'<div id="name">You</div>'+
							'</div>';
                angular.element(document.getElementById("wrapper")).append(html);

				//clear 
				angular.element(document.getElementById("message")).value = "";
			}

			vm.logoutAccount = function() {
				conn.onClose();
				console.log('disconnected');
				$state.go('login');
			}

		}]);
}) ();