(function() {

	angular.module('UserService', [])
	.factory('UserService', ['$q', '$http',
		function($q,$http) {

			return {
				checkUser:checkUser,
			
			}

			function checkUser(data) {
			var defer = $q.defer();
			$http.post('app/model/User.php', data)
			.then(function(response) {
				defer.resolve(response.data);

			}).catch(function(error) {
					defer.reject(response.data);
			}); 
			return defer.promise;
		}
			


		}]);

})();
