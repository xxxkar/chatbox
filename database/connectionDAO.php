<?php

	class ConnectionDAO {

		//FOR LOCAL HOST
		protected $username = 'root';
		protected $password = '';
		protected $db_name = 'sakura_chatbox';
		protected $host = '127.0.0.1';

		protected $dbh = null; //pdo container

		public function openConnection() {
			try {
				$this->dbh = new PDO("mysql:host=". $this->host . ";dbname=". $this->db_name, $this->username, $this->password);
				$this->dbh->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
				$this->dbh->exec('SET NAMES utf8');
			} catch(PDOException $e) {
				echo $e->getMessage();
			}
		}

		public function closeConnection() {
			try {
				$this->dbh = null;
			} catch(PDOException $e) {
				$e->getMessage();
			}
		}

	} 

