<?php
	require __DIR__ . '/connectionDAO.php';

	class userDAO extends ConnectionDAO {

		public function fetchUser($username,$password) {
	
			$query = "SELECT * from `users` WHERE `username`= ? AND `password`= ?";
			$returnData = null;

			try {
				$this->openConnection();
				$stmt = $this->dbh->prepare($query);
				$stmt->bindparam(1, $username);
				$stmt->bindparam(2, $password);
				$stmt->execute();

				if($res = $stmt->fetch(PDO::FETCH_ASSOC)) {
					$returnData = [
						'status' => 'login',
						'username' => $res['username']
					];
				}

				else {
					$returnData = ['status' => 'register'];
				}
				
				return $returnData;

			} catch(PDOException $e) {
				return $e->getMessage();
			}
		}

		public function createUser($data) {

			$query = "INSERT INTO `users` (`username`,`password`) VALUES (?,?)";
			$returnData = null;

			try {
				$this->openConnection();
				$stmt = $this->dbh->prepare($query);
				$stmt->bindparam(1, $data['username']);
				$stmt->bindparam(2, $data['password']);
				$stmt->execute();

				$ctr = $stmt->rowCount();

				if($ctr > 0) {
					$returnData = [
						'status' => 'registered',
					];
				}
				else {
					$returnData = ['status' => "not register"];
				}
				
				return $returnData;

			} catch(PDOException $e) {
				return $e->getMessage();
			}

		}
		
	}

?>